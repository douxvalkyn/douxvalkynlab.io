---
title: About us
subtitle: Qui sommes nous ?
comments: true
bigimg: [{src: "/guilhem/guilhem/img/triangle.jpg", desc: "Triangle"}, {src: "/guilhem/guilhem/img/sphere.jpg", desc: "Sphere"}, {src: "/guilhem/guilhem/img/hexagon.jpg", desc: "Hexagon"}]
---

![portrait](/guilhem/guilhem/img/about.jpg)


### Que raconte ce blog ?

Nous essayons de garder une trace des premiers mois de notre petit Guilhem, né le 27 juin 2022.

### Qui réalise ce blog ?

Les textes sont de Aude et les photos ainsi que les vidéos sont principalement de Régis. La conception et la maintenance du site sont entre les mains de Régis.