---
title: Les meilleures vidéos
subtitle: 
comments: true
bigimg: [{src: "/guilhem/guilhem/img/triangle.jpg", desc: "Triangle"}, {src: "/guilhem/guilhem/img/sphere.jpg", desc: "Sphere"}, {src: "/guilhem/guilhem/img/hexagon.jpg", desc: "Hexagon"}]
---



### Liste des 20 meilleures vidéos

Le nounours (2 mois ):
{{< youtube 3q-95GN5uVQ >}}

Le mixeur (3 mois):
{{< youtube vpbGXwz7l_g >}}

Le miroir (3 mois):
{{< youtube L9TVIsFt_RU >}}

La balle (4 mois):
{{< youtube zSVxVcRvw2s >}}

Le coucou caché (7 mois):
{{< youtube pTaEDUDbL-g >}}

La pomme (8 mois):
{{< youtube 5s3_DWucEO8 >}}

Le chien et le chat (10 mois):
{{< youtube -buHhiT1ckY >}}

Premiers pas ( 12 mois):
{{< youtube T6ebwjNm-OA >}}

Dans le bain 1 ( 12 mois):
{{< youtube fYi9LVbHVH0 >}}

Le puzzle (13 mois):
{{< youtube 7vrnmjuq5NY >}}

Danse avec un gâteau (16 mois):
{{< youtube CZ-IfgYW_Cs >}}

Nounours au supermarché (18 mois):
{{< youtube 2Bv6xnDMcjY >}}

Dans le bain 2 (20 mois):
{{< youtube 8ZdNQwY1JIw >}}

Les cubes (23 mois):
{{< youtube YXfrLIu-38U >}}

Danse en vacances (2 ans):
{{< youtube 9CD4p5E0BEE >}}

Danse encore en vacances (2 ans):
{{< youtube zlYWIbY3QKA >}}

La plage avec maman (2 ans):
{{< youtube QJLCsfA_oGI >}}














