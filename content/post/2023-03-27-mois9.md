---
title: Le neuvième mois
#subtitle: 
date: 2023-03-27
tags: ["guilhem"]
author: "Régis"
imageMini: "guilhem/img/mois9/641e9befd9be9b0fd71j_tn.jpg"
#image:
---

Un neuvième mois de mars sous un très bon signe pour Guilhem !

A la mi-février, il a enfin commencé à faire des nuits complètes (avec encore quelques réveils une fois sur deux, mais qui ne duraient jamais très longtemps), et, à la reprise du travail fin février, il ne se réveillait plus du tout. Un bonheur ! En me couchant de 21h30 à 7h30, j'ai même parfois fait des nuits trop longues ! Quelle ironie. Nous pouvions enfin souffler après 8 longs mois d'attente.

Nous avons tout de même maintenu notre rendez-vous de suivi avec Clémence début mars, même si je n'avais plus aucune questions à lui poser, histoire de "clôturer" le suivi.

A la crèche, il y a eu de belles améliorations également : je m'arrangeais pour qu'il fasse de petites journées pour la première semaine de reprise car il y a toujours une sieste sur les deux qui est trop courte (30min), et il a parfois besoin de récupérer en faisant une troisième sieste à la maison. Les deux semaines suivantes se sont mieux déroulées et il était enfin de bonne humeur et joueur. Cela faisait plaisir à voir ! Le sommeil, même s'il n'est pas aussi parfait qu'à la maison, est tout de même nettement mieux désormais à la crèche, et il pleure beaucoup moins en journée. Là aussi, après 3 mois, il commence tout juste à s'adapter et se sentir à l'aise...

Seuls l'appétit et le poids ne sont toujours pas au top :/ Nous avons revu le Dr Levy début mars mais le poids n'avait toujours pas bougé. Mais bon, tant qu'il est toujours tonique et souriant, nous ne nous inquiétons pas trop.

Sinon, petite anecdote : j'ai dû passé une journée entière au CHU de Nantes (après avoir vu SOS médecin en urgence la veille au soir), à cause de fortes douleurs au ventre. J'angoissais car je laissais le petit toute la journée à Régis, mais finalement tout s'est très bien passé, le petit a bien joué, bien dormi et bien mangé. Et moi, je suis revenue bredouille : malgré l'échographie et les prises de sang, les médecins n'ont rien trouvé de particulier (pas de kyste ovarien ni d'appendicite). La gynécologue évoquait une ovulation douloureuse pour la reprise du cycle. J'espère que ça ne se reproduira pas en tout cas, car c'est très angoissant de ne plus être opérationnelle pour s'occuper du petit !

Côté progrès moteurs : Guilhem est toujours aussi explorateur à la maison et adore se promener dans les différentes pièces de la maison. Son passe temps favori est de se hisser sur les pots de fleurs pour se relever (et de manger la terre au passage !). Aussi, Régis lui fait des petits parcours dans l'appartement, en semant un peu partout les marionnettes animaux. Guilhem adore aller les chercher !

C'est un vrai petit coquin, plein de malice : il fait tout ce qui est interdit et se marre en me voyant le gronder (comme manger la terre, mordre très fort mes doigts, faire tomber les objets - comme les pommes !-, recracher sa purée en postillonnant...). Il fait tout cela en connaissance de cause car il rigole juste après l'avoir fait ! C'est un petit filou.

Un autre progrès moteur à noter ce mois-ci : il a réussi à se mettre en position assise tout seul, en étant en position allongée !

La fin du mois de mars a été un peu plus désordonnée côté nuits : comme Guilhem ne mangeait presque rien parfois le soir, il s'est réveillé au début de sa nuit et j'ai tout de suite pensé qu'il avait faim, donc je l'ai fait téter. Mais cela a duré plusieurs nuits (3-4), et comme Clémence me l'avait dit, il ne faut pas faire marche arrière dans les réveils nocturnes ! Au risque de le réhabituer à téter pour se rendormir... (ça ne doit rester qu'exceptionnel). J'ai senti que cela l'avait perturbé dans ses nuits, car il recommençait à se réveiller sans raison (avec à nouveau des insomnies)... Donc j'ai vite rectifié le tir et, quand j'étais sûre qu'il avait assez mangé la veille, j'ai cessé de le nourrir, même si cela a occasionné des pleurs (et réveillé la voisine au passage...).

{{< gallery dir="guilhem/img/mois9/" thumb="_tn" />}}


---
Quelques vidéos :

{{< youtube 5s3_DWucEO8 >}}
{{< youtube XPlLi5s5KyM >}}
{{< youtube Kmp8ExrYvEs >}}
{{< youtube egaun7BubSw >}}

