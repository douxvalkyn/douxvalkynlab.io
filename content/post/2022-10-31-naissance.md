---
title: La naissance
subtitle: Le 27 juin 2022
date: 2022-10-31
tags: ["guilhem", maternité]
author: "Régis"
imageMini: "guilhem/img/naissance/62bf2dc1a18db2d9d8dj_tn.jpg"
#image:
---

Les contractions ont commencé le samedi soir et se sont prolongées jusqu'à dimanche soir. Elles étaient supportables. Dans la nuit du dimanche à lundi, elles sont devenues lis intenses et plus régulières. Je courais partout dans l'appart en attendant que ça passe ^^ Puis on est partis à la clinique le lundi matin à 9h.



Arrivés à la clinique, j'étais déjà en début de travail avec 4cm d'ouverture. La sage femme m'a proposé la baignoire avant la péridurale et j'ai accepté. J'y suis restée 2 heures. C'était agréable, même si les contractions restaient douloureuses.

Puis j'ai commencé la péridurale après le bain car j'étais rendue à 5-6cm et que je commençais à fatiguer.

La péridurale m'a bien soulagée, si j'avais su je l'aurais prise plus tôt...J'ai pu me reposer une bonne heure et le travail a tranquillement continué. Jai fini par être à 8cm. Mais l'hypoglycémie (car je n'avais pas le droit de mangé et j'avais l'estomac vide puis 8h), m'a donné des malaises et des renvois. Il ne fallait pas que ça dure trop longtemps sinon je n'aurais plus de force pour la poussée.... La sage femme a donc rompu elle-même la poche des eaux et m'a mise sur le côté afin que le bébé pousse sur le col de l'utérus. C'était assez douloureux donc j'ai demandé à ce qu'elle augmente la dose de la péridurale.

Je commençais sur la fin à fatiguer et à ne plus avoir d'énergie donc on a commencé la poussée une fois que le col était dilaté à 10cm. Mais la poussée a été moins efficace car j'avais trop dosé la péridurale... De plus, la tête de Guilhem avait du mal à passer donc il a fallu utiliser une ventouse pour la faire pivoter dans le bassin.

A force, la tête puis le corps ont fini par sortir. J'ai eu des déchirures internes au vagin et externes, sur quelques centimètres. Je n'avais aucune douleur grâce à la péridurale. Lors de la naissance, j'ai été très émue en voyant mon bébé en vrai.

Regis l'a pris en peau à peau le temps que le gynécologue me recouse.

C'était émouvant, et on en a bien profité (même trop, car on a dû attendre au moins 2h avant d'être ramené en chambre et nourri, or j'avais terriblement faim... !).

{{< gallery dir="guilhem/img/naissance/" thumb="_tn" />}}


