---
title: Le quatrième mois
#subtitle: 
date: 2022-11-06
tags: ["guilhem"]
author: "Régis"
imageMini: "guilhem/img/mois4/63408bba1468022a1e5j_tn.jpg"
#image:
---

Ce 4e mois d'octobre a été très intense physiquement et nerveusement. Les pleurs de Guilhem et son besoin d'être porté n'ont pas diminué au fil du temps comme nous l'avions espéré (ils se sont même accrus !), et les nuits nous ont achevé, avec en moyenne 5 levers par nuit + une longue pause au milieu d'une heure ou deux...

Cela me décourage un peu car je pensais qu'une fois la succion réglée j'allais enfin pouvoir respirer un peu. Mais non, Guilhem souffre toujours beaucoup après les tétées et a besoin d'être porté au moins 1h pour évacuer tous ses rots. Comment aurait-il alors supporté la position allongée en crèche ? Il aurait souffert le martyre ! Au moins je suis vraiment soulagée d'avoir pu le garder pour lui avoir évité tout ça.

Les groupes facebook (RGO + allaitement) ont été d'une aide précieuse moralement : ils m'ont aidé à comprendre certains problèmes et ont apporté quelques solutions :

- porter +++ et ne pas laisser allongé après la tétée

- donner du gaviscon après les tétées pour éviter les remontées acides (ne fonctionne que très rarement ou si pleur très intenses)

- changer la vitamine Zyma D pour une plus naturelle (pediakids)

- donner des probiotiques (biogaia)

- incliner le lit

- donner du pediakids gaz ou du julep gommeux contre les coliques

- tenter l'éviction des PLV

...et éventuellement passer à la solution médicamenteuse de l'Inexium si rien ne fait effet.

Mais malgré tous ces conseils, rien n'a été miraculeux. Je ne suis même pas sûre qu'il souffre réellement de RGO... J'ai repris rendez-vous avec l'ostéopathe Alix de La Forge, sur conseil d'Andréa l'orthophoniste, mais ça n'a rien changé. J'ai finalement décidé de commencer l'éviction totale des Protéines de lait de vache (PLV) de mon alimentation - avec toutes ses allergies croisées (soja, chèvre, brebis) - car l'intolérance au lactose est souvent responsable des problèmes digestifs des nourrissons. Ca a été un crève cœur au début mais j'étais prête à tout pour soulager Guilhem ! Mais après 2 semaines de régime, rien n'a franchement changé... Au mieux j'ai constaté moins de renvois après les tétées, mais Guilhem avait encore beaucoup de rots coincés et ce, même en ingérant d'autres liquides que mon lait (comme le mélange au fenouil pediakids). C'est donc que ça doit venir de la manière dont il le digère à cause de l'immaturité de son système digestif (c'est mécanique), et qu'on n'y peut rien... Cela m'encourage à prendre mon mal en patience et à me dire que ça va finir par passer.

L'autre souci, c'est aussi qu'il ne fait pas du tout ses nuits (ça s'est même empiré au fil du 4e mois, on est rendus à un réveil toute les heures voire moins X_X !) Il se réveille pour des raisons diverses : tétées, gênes (rots), envie de faire caca... Et moi qui croyais naïvement qu'à 3 mois il ferait ses nuits ^^'

Bref, on pourrait croire que tout va mal , mais il y a quand même eu de beaux moments ce mois-ci : Guilhem rit toujours énormément et progresse de jour en jour dans sa motricité (il tient presque assis seul et tient bien sa tête) 😀 C'est un petit garçon adorable qui suscite l'admiration de tout le monde quand on sort et qui a toujours un regard très vif et très curieux quand il voit de nouvelles personnes. On a même investi dans un nouveau porte bébé (l'ergobaby 360) afin que Guilhem puisse enfin être face au monde, sa position préférée !
{{< gallery dir="guilhem/img/mois4/" thumb="_tn" />}}


---
Quelques vidéos :

{{< youtube 5CvCAwT9UNw >}}
{{< youtube P35N4kkuDPI >}}
{{< youtube zdua6A_e-A4 >}}
{{< youtube 4t-ahBU8928 >}}