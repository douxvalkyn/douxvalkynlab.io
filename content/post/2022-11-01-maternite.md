---
title: Les premiers jours à la maternité
#subtitle: 
date: 2022-11-01
tags: ["guilhem", "maternité"]
author: "Régis"
imageMini: "guilhem/img/maternite/62bf3f1f51c587e1ff4j_tn.jpg"
#image:
---

Les jours qui ont suivi à la maternité ont été merveilleux ! Et ce, malgré les quelques douleurs post accouchement.

Guilhem a tout de suite été mis au sein pour stimuler ma lactation et prendre le colostrum. Mais j'ai eu très rapidement mal aux mamelons car ça tiraillait... La sage femme m'a aidé en me donnant un bout de sein en silicone. Ajouté à ça, la montée de lait a énormément fait gonfler mes seins. Ils sont devenus énormes et tout durs... Il a fallu que Guilhem tète bien régulièrement pour les désengorger. Ça a pris plusieurs jours. C'était douloureux, il a fallu prendre son mal en patience...

Concernant les autres douleurs, j'avais du mal à me lever et à marcher au début, et j'avais des brulures au périnée suite à mes déchirures ainsi que des douleurs dans le dos au niveau de la piqûre de la péridurale. Je restais alitée tous les jours.

Cependant, je me sentais aussi soulagée d'un poids, mon ventre avait bien dégonflé et je pouvais enfin m'allonger sans avoir mal aux côtes. De plus, l'œdème aux mollets avait instantanément disparu. C'était un vrai soulagement de "retrouver" mon corps !!

Concernant notre séjour à la maternité, nous étions sur un petit nuage. Régis était là tout le jour et les sages femmes et puéricultrices passaient toute la journée s'occuper de moi et du bébé. On était vraiment chouchoutés ! En plus, elles me donnaient beaucoup de conseils et m'aidaient à l'allaitement. Si besoin, la nuit, je pouvais leur laisser Guilhem à la nurserie afin de me reposer un peu. Les repas étaient sans plus mais je mangeais avec appétit. La chambre était spacieuse (ce n'était pas la simple chambre individuelle car elle était accolée à la chambre pour le conjoint et avait une superbe terrasse au calme). C'était très agréable, je n'étais même pas pressée de partir.

En bref, un fabuleux moment, où nous étions sur un petit nuage et n'avons pas vu le temps passer !

{{< gallery dir="guilhem/img/maternite/" thumb="_tn" />}}


