---
title: Accueil
subtitle: Home
date: 2022-11-01
tags: ["Guilhem"]
---


![portrait accueil](guilhem/img/photo_accueil3.jpg)


<!---
# Pour placer une (ou plusieurs (=tournantes)) big image en haut de page, il suffit de placer cette ligne de code dans l'entete d'une page:
bigimg/: [{src: "guilhem/img/triangle.jpg", desc: "Triangle"}]
bigimg/: [{src: "guilhem/img/triangle.jpg"},  {src: "guilhem/img/sphere.jpg"},  {src: "guilhem/img/hexagon.jpg"}]



# Pour utiliser une galerie d'images:
For full details please see the [hugo-easy-gallery GitHub](https://github.com/liwenyip/hugo-easy-gallery/) page. Basic usages from above are:

- Create a gallery with open and close tags `{{</* gallery */>}}` and `{{</* /gallery */>}}`
- `{{</* figure src="image.jpg" */>}}` will use `image.jpg` for thumbnail and lightbox
- `{{</* figure src="thumb.jpg" link="image.jpg" */>}}` will use `thumb.jpg` for thumbnail and `image.jpg` for lightbox
- `{{</* figure thumb="-small" link="image.jpg" */>}}` will use `image-small.jpg` for thumbnail and `image.jpg` for lightbox
- All the [features/parameters](https://gohugo.io/extras/shortcodes) of Hugo's built-in `figure` shortcode work as normal, i.e. src, link, title, caption, class, attr (attribution), attrlink, alt
- `{{</* gallery caption-effect="fade" */>}}` will fade in captions for all figures in this gallery instead of the default slide-up behavior
- Many gallery styles for captions and hover effects exist; view the [hugo-easy-gallery GitHub](https://github.com/liwenyip/hugo-easy-gallery/) for all options

{{< gallery caption-effect="fade" thumb="_tn" >}}
  {{< figure src="guilhem/img/PXL_20221008_100140151.jpg" >}}
  {{< figure src="guilhem/img/PXL_20221009_145915746.jpg" caption="Sphere" >}}
  {{< figure src="guilhem/img/PXL_20221009_162504578.jpg" caption="Triangle"  >}}
{{< /gallery >}}
--->

