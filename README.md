Tutorial sur internet, pour la mise en place avec Gitlab, Hugo, Git:
https://blog.deniger.net/post/hugo-and-gitlab/

---
## Modifications du projet 

Pour modifier et mettre à jour le site https://douxvalkyn.gitlab.io/guilhem
- les modifications sont à faire sous: `C:\hugo\Sites\guilhem\content` au format `.md`
- visualiser en local: open git bash ici à la racine: `C:\hugo\Sites\guilhem`

Taper: `hugo server -D`

Les modifications sont alors visibles en temps réel ici: `http://localhost:1313/`

Ensuite les petites modifications sont visibles immédiatement, mais les ajouts de contenu doivent être suivis d'un `hugo server -D`

- envoyer les modifications sur gitlab: open git bash ici à la racine: `C:\hugo\Sites\guilhem`

Taper: `git add .  `

Taper: `git commit -a -m "message de modif"`

Taper: `git push`

- Voir les modifications sur Gitlab:  le pipeline est automatiquement lancé: https://gitlab.com/douxvalkyn/guilhem/-/pipelines

Si tout est ok, les modifications sont visibles ici sur internet: https://douxvalkyn.gitlab.io/guilhem

Pour créer les thumbnails des images (mettre 200x200 pixels):
http://makethumbnails.com/#dropzone

---
## Modifier le thème

Pour mettre à jour le thème et permettre des modifications du css par exemple, il faut effectuer des modifications dans le projet douxvalkyn/beautifulhugo.

Ensuite, il faut rapatrier ces modifications dans le projet Guilhem car cela ne semble pas fait automatiquement. Il semble nécessaire de supprimer le dossier  themes/beautifulhugo2 en local, puis de ré-importer le theme depuis le dossier *themes* via la commande:

 `git submodule add https://gitlab.com/douxvalkyn/beautifulhugo.git beautifulhugo2`

Une fois le dossier de nouveau sous themes/beautifulhugo2, il faut envoyer ces changements sur gitlab:

 - `git add .  `
 - `git commit -a -m "message de commit"  `
 - `git push  `

---
## Modifications de l'url sur gitlab pages
- url finale: https://douxvalkyn.gitlab.io
  - sur gitlab, modifier le path: : https://gitlab.com/douxvalkyn/`douxvalkyn.gitlab.io`
  - modifier l'arborescence du projet pour mettre les images dans un dossier "img": guilhem / static /img
  - dans config.toml: baseURL= "https://douxvalkyn.gitlab.io"
  - exemple d'un lien vers une image:

  `![portrait accueil](img/photo_accueil3.jpg)`



- url finale: https://douxvalkyn.gitlab.io/guilhem/
  - sur gitlab, modifier le path: : https://gitlab.com/douxvalkyn/`guilhem`
  - modifier l'arborescence du projet pour mettre les images dans un dossier "guilhem/img": guilhem / static /guilhem /img
  - dans config.toml: baseURL= "https://douxvalkyn.gitlab.io/guilhem"
  - modifier les liens dans le code pour les éléments qui sont dans le dossier img afin d'indiquer qu'ils sont désormais dans le dossier guilhem/img, exemple:

  avant: `![portrait accueil](img/photo_accueil3.jpg)`

  après:  `![portrait accueil](guilhem/img/photo_accueil3.jpg)`

  _Remarque_: attention, les liens vers les images sont différents entre la page index et les autres pages.
